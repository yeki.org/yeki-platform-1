# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
from helpers.ssm import SSM

ssm = SSM('/yeki/production')

STATIC_URL = '/static/'
STATIC_ROOT = 'static'

AWS_S3_REGION_NAME = ssm.get_param(
    '/yeki/production/storage/region'
)
AWS_STORAGE_BUCKET_NAME = ssm.get_param(
    '/yeki/production/storage/bucket_name'
)
AWS_ACCESS_KEY_ID = ssm.get_param(
    '/yeki/production/storage/access_key_id'
)
AWS_SECRET_ACCESS_KEY = ssm.get_param(
    '/yeki/production/storage/secret_access_key'
)
AWS_S3_CUSTOM_DOMAIN = ssm.get_param(
    '/yeki/production/storage/custom_domain'
)

AWS_DEFAULT_ACL = None

# Tell the staticfiles app to use S3Boto3 storage when writing the
# collected static files (when you run `collectstatic`).
STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'helpers.storages.StaticStorage'

MEDIAFILES_LOCATION = 'media'
DEFAULT_FILE_STORAGE = 'helpers.storages.MediaStorage'
